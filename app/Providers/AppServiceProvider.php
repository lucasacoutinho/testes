<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }

        Request::macro('isAdmin', function () {
            return $this->getHost() === adminUrl();
        });
    }
    
    public function boot()
    {
        if(App::environment('local')){
            DB::listen(function ($query) {
                $conteudoLog = PHP_EOL;
                $conteudoLog .= '-- [' . now()->toDateTimeString() . ']';
                $conteudoLog .= PHP_EOL;
                $conteudoLog .= $query->sql;
                $this->substituirParametrosLogSql($conteudoLog, $query->bindings);
                $conteudoLog .= ';';
                $conteudoLog .= PHP_EOL;
                $conteudoLog .= "-- Tempo: {$query->time}ms";
                Storage::append('log.sql', $conteudoLog);
            });
        }
    }

    private function substituirParametrosLogSql(&$conteudoLog, $parametros)
    {
        foreach ($parametros as $parametro) {
            $posicaoSubstituicao = strpos($conteudoLog, '?');
            $conteudoLog         = substr_replace($conteudoLog, "'$parametro'", $posicaoSubstituicao, 1);
        }
    }

}
